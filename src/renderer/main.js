import Vue from "vue";
import axios from "axios";

import App from "./App";
import router from "./router";
import store from "./store";
import Vuetify from "vuetify";

import "vuetify/dist/vuetify.css";
import "@mdi/font/css/materialdesignicons.css";
import VueFire from 'vuefire';



Vue.use(Vuetify, {
  iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4'
});
Vue.use(VueFire);

// if (!process.env.IS_WEB) Vue.use(require("vue-electron"));
// Vue.http = Vue.prototype.$http = axios;
// Vue.config.productionTip = false;


/* eslint-disable no-new */
process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = "true";


if (window.localStorage.token && window.localStorage.token != "undefined")
  store.dispatch("setToken", window.localStorage.token);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");