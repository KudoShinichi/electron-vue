import Vue from "vue";
import VueRouter from "vue-router";

import store from "../store/index.js";

import Main from "../components/Main.vue";
import Home from "../view/Home.vue";
import Menu from "../view/Menu.vue";
import Login from "../view/Login.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [{
      path: "/Login",
      name: "Login",
      component: Login
    },
    {
      path: "/",
      component: Main,
      meta: {
        auth: true
      },
      children: [{
          path: "/",
          name: "Home",
          component: Home
        },
        {
          path: "/menu",
          name: "Menu",
          component: Menu
        }
      ]
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (to.matched.meta || to.matched[0].meta.auth) {
    store.state.token ?
      next() :
      next({
        name: "Login"
      });
  } else {
    next();
  }
});
export default router;