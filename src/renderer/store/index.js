import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		token: null,
	},
	mutations: {
		SET_TOKEN: (state, token) => {
			state.token = token;
		},
	},
	actions: {
		setToken({
			commit
		}, val) {
			commit("SET_TOKEN", val);
			window.localStorage.token = val;
		},
	}
});
export default store;