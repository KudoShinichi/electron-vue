import Firebase from "firebase";

const firebaseApp = Firebase.initializeApp({
  apiKey: "AIzaSyDk0Dj3N1SS4mvUbgpxZmbMmEwLLSiXBNM",
  authDomain: "fir-electron-3a840.firebaseapp.com",
  databaseURL: "https://fir-electron-3a840.firebaseio.com",
  projectId: "fir-electron-3a840",
  storageBucket: "fir-electron-3a840.appspot.com",
  messagingSenderId: "1003519211500"
});

export const db = firebaseApp.firestore();
export const auth = firebaseApp.auth();